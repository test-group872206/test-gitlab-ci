from typing import Any
from database import session
from objects import Formula, Recipe
from schemas import RecipeIn
from sqlalchemy import update
from sqlalchemy.future import select


async def get_all_recipes() -> Recipe:
    res = await session.execute(
        select(Recipe).order_by(Recipe.views & Recipe.avg_time)
    )
    return res.scalars().all()


async def get_recipe_by_id(id: int) -> Any:
    async with session.begin():
        formula = await session.execute(
            select(Formula).filter(
                Formula.recipe_id == id, Recipe.id == Formula.recipe_id
            )
        )
        recipe = await session.execute(
            select(Recipe).filter(
                Formula.recipe_id == id, Recipe.id == Formula.recipe_id
            )
        )
        recipe = recipe.scalars().one_or_none()
        formula = formula.scalars().one_or_none()
        if recipe and formula:
            formula = formula.to_json()
            formula["title"] = recipe.title
            formula["views"] = recipe.views
            formula["avg_time"] = recipe.avg_time
            await session.execute(
                update(Recipe)
                .where(Recipe.id == id)
                .values({Recipe.views: Recipe.views + 1})
            )
    return formula


async def create_recipe(recipe_info: RecipeIn):
    async with session.begin():
        recipe_data = {
            "title": recipe_info.title,
            "views": recipe_info.views,
            "avg_time": recipe_info.avg_time,
        }
        new_recipe = Recipe(**recipe_data)
        session.add(new_recipe)
    async with session.begin():
        formula_data = {
            "recipe_id": new_recipe.id,
            "ingredients": recipe_info.ingredients,
            "description": recipe_info.description,
        }
        new_full_recipe = Formula(**formula_data)
        session.add(new_full_recipe)
    return new_recipe.id
