from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship


class Recipe(Base):
    __tablename__ = "recipe"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    views = Column(Integer, index=True, default=0, nullable=False)
    avg_time = Column(Integer, nullable=False)
    formulas = relationship(
        "Formula", cascade="all, delete-orphan", lazy="select"
    )


class Formula(Base):
    __tablename__ = "formula"

    id = Column(Integer, primary_key=True)
    recipe_id = Column(
        Integer, ForeignKey("recipe.id"), nullable=False, index=True
    )
    ingredients = Column(String, nullable=False)
    description = Column(String, nullable=False)

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
