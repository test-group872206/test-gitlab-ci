from pydantic import BaseModel, Field


class RecipeModel(BaseModel):
    title: str = Field(..., title="Название блюда.")
    views: int = Field(..., title="Количество просмотров рецепта.")
    avg_time: int = Field(
        ..., title="Ориентировочное время " "приготовление блюда."
    )


class RecipeIn(RecipeModel):
    ingredients: str = Field(..., title="Ингредиенты блюда.")
    description: str = Field(..., title="Подробное описание рецепта/блюда.")


class RecipeOut(RecipeModel):
    id: int = Field(..., title="ID рецепта в БД.")

    class Config:
        orm_mode = True
