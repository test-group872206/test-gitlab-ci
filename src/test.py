from app import app
from fastapi.testclient import TestClient

client = TestClient(app)


def test_create_empty_recipe():
    response = client.post(
        "/recipes/",
        json={"title": "Окрошка"},
    )
    assert response.status_code == 422


def test_create_recipe_bad_args():
    response = client.post(
        "/recipes/",
        json={
            "title": "string",
            "views": "fasafas",
            "avg_time": 0,
            "ingredients": "string",
            "description": "string",
        },
    )
    assert response.status_code == 422


def test_create_recipe():
    response = client.post(
        "/recipes/",
        json={
            "title": "Окрошка",
            "views": 2024,
            "avg_time": 35,
            "ingredients": "Картофель, кефир, яйца, колбаса.",
            "description": "Домашняя окрошка на кефире, с варенной колбасой.",
        },
    )
    assert response.status_code == 200
    assert response.json() == {"message": "Создан новый рецепт с id 1."}


def test_get_recipes():
    response = client.get("/recipes/")
    assert response.status_code == 200
    assert response.json()
