from typing import List
import uvicorn  # type: ignore
from database import Base, engine, session
from fastapi import FastAPI, Path
from models import create_recipe, get_all_recipes, get_recipe_by_id
from schemas import RecipeIn, RecipeOut

tags_metadata = [
    {
        "name": "all",
        "description": "Данный эндпоинт возвращает все "
        "существующие рецепты в БД.",
    },
    {
        "name": "by_id",
        "description": "Данный эндпоинт возвращает подробную "
        "информацию о конкретном рецепте.",
    },
    {
        "name": "add",
        "description": "Через данный эндпоинт создаются новые рецепты.",
    },
]

app = FastAPI(openapi_tags=tags_metadata)


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get("/recipes/", response_model=List[RecipeOut], tags=["all"])
async def get_recipes() -> List[RecipeOut]:
    return await get_all_recipes()


# flake8: noqa: B008
@app.get("/recipes/{id}", tags=["by_id"])
async def get_recipe(
    recipe_id: int = Path(
        title="Введите ID рецепта, информацию о " "котором нужно посмотреть.",
        default=1,
    ),
) -> dict:
    return await get_recipe_by_id(recipe_id)


@app.post("/recipes/", tags=["add"])
async def add_recipe(recipe_info: RecipeIn) -> dict:
    recipe_id = await create_recipe(recipe_info)
    return {"message": f"Создан новый рецепт с id {recipe_id}."}


if __name__ == "__main__":
    uvicorn.run("app:app", port=5000, host="127.0.0.1")
